<div class="card text-white bg-primary mb-4">
    <div class="card-header">
        <?= $title ?>
    </div>
    <div class="card-body">
        <?= $sidebarContent ?>
    </div>
</div>