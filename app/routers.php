<?php

use app\core\Controller;
use app\core\QueryBuilder;

// Router::get('/', 'HomeController@index');
Router::get('/home', 'HomeController@index');

Router::get('/', function () {
    $builder = QueryBuilder::table('123')
        ->select('col1', 'col2')
        ->distinct()
        ->orderBy('cot1', 'ASC')
        ->orderBy('cot2', 'DESC')
        ->limit(10)
        ->offset(3)
        ->get();
    echo "<pre>";
    print_r($builder);
});

Router::any('*', function () {
    echo "404 Not found";
});
