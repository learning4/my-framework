<?php

use app\core\Registry;

require_once(dirname(__FILE__) . '/Autoload.php');

/**
 * App
 */
class App
{
    private $router;

    public function __construct($config)
    {
        new Autoload($config['rootDir']);

        $this->router = new Router;

        Registry::getInstance()->config = $config;
    }

    public function run()
    {
        $this->router->run();
    }
}
