<?php

use app\core\AppException;
use app\core\Registry;

/**
 * Router
 */
class Router
{
    private static $routers = [];

    public function __construct()
    {
        // code...
    }

    private function getRequestURL()
    {
        $url = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '';
        if (($key = stripos($url, 'public')) !== false && $key > 1) {
            $url = str_split($url, $key + 6)[1];
        }

        return $url;
    }

    private function getRequestMethod()
    {
        $metohod = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : 'GET';
        return $metohod;
    }

    private static function addRouter($method, $url, $action)
    {
        self::$routers[] = [$method, $url, $action];
    }

    public static function get($url, $action)
    {
        self::addRouter('GET', $url, $action);
    }

    public static function post($url, $action)
    {
        self::addRouter('POST', $url, $action);
    }

    public static function any($url, $action)
    {
        self::addRouter('GET|POST', $url, $action);
    }

    public function map()
    {
        $requestURL = $this->getRequestURL();
        $requestMethod = $this->getRequestMethod();

        $routers = self::$routers;

        $checkRoute = false;
        $params = [];

        foreach ($routers as $route) {
            list($method, $url, $action) = $route;


            if (strpos($method, $requestMethod) === false) {
                continue;
            }

            if ($url === '*') {
                $checkRoute = true;
            } elseif (strpos($url, '{') === false) {
                if (strcmp(strtolower($url), strtolower($requestURL)) === 0) {
                    $checkRoute = true;
                } else {
                    continue;
                }
            } elseif (strpos($url, '}') === false) {
                continue;
            } else {
                $routeParams    = explode('/', $url);
                $requestParams  = explode('/', $requestURL);

                if (count($routeParams) !== count($requestParams)) {
                    continue;
                }

                foreach ($routeParams as $k => $rp) {
                    if (preg_match('/^{\w+}$/', $rp)) {
                        $params[] = $requestParams[$k];
                    }
                }

                $checkRoute = true;
            }

            if ($checkRoute === true) {
                if (is_callable($action)) {
                    call_user_func_array($action, $params);
                    break;
                } elseif (is_string($action)) {
                    $this->compileRoute($action, $params);
                    break;
                }
            }
        }
    }

    private function compileRoute($action, $params)
    {
        if (count(explode('@', $action)) !== 2) {
            die('Route error');
        }

        $className = explode('@', $action)[0];
        $methodName = explode('@', $action)[1];

        $classNamespace = 'app\\controllers\\' . $className;

        if (class_exists($classNamespace)) {
            $object = new $classNamespace;

            Registry::getInstance()->controller = $className;

            if (method_exists($classNamespace, $methodName)) {
                Registry::getInstance()->action = $methodName;

                call_user_func_array([$object, $methodName], $params);
            } else {
                throw new AppException("Method " . $methodName . " not found");
            }
        } else {
            throw new AppException('Controller ' . $classNamespace . ' not found');
        }
    }

    public function run()
    {
        $this->map();
    }
}
