<?php

use app\core\AppException;

/**
 * Autoload
 */
class Autoload
{
    private $rootDir;
    public function __construct($rootDir)
    {
        $this->rootDir = $rootDir;

        spl_autoload_register([$this, 'autoload']);

        $this->autoLoadFile();
    }

    private function autoload($class)
    {
        $classArr = explode('\\', $class);
        $className = end($classArr);
        $pathName = str_replace($className, '', $class);

        $filePath = $this->rootDir . '\\' . $pathName  . $className . '.php';

        $filePath = str_replace('\\', '/', $filePath);

        if (file_exists($filePath)) {
            require_once($filePath);
        } else {
            throw new AppException("{$className} is not exists");
        }
    }

    private function autoLoadFile()
    {
        foreach ($this->defaultFileLoad() as $file) {
            require_once($this->rootDir . '/' . $file);
        }
    }

    private function defaultFileLoad()
    {
        return [
            'app/core/Router.php',
            'app/routers.php',
        ];
    }
}
