<?php

namespace app\controllers;

use app\core\Controller;
use \App;

/**
 * HomeController
 */
class HomeController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->render('index', [
            'ten' => 'Tai',
            'tuoi' => 22,
        ]);
    }
}
